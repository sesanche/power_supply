/*!
 * \authors Monica Scaringella <monica.scaringella@fi.infn.it>, INFN-Firenze
 * \date July 24 2020
 */

// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <boost/program_options.hpp> //!For command line arg parsing
#include <errno.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>

// //Custom Libraries
// #include "CAENelsFastPS.h"
// #include "Keithley2410.h"
// #include "TTi_PL.h"

// #include "Keithley2000.h"
// #include "KeithleyMultimeter.h"

//#define PLOT // to create graph, requires gnuplot
#define SAVE // to save figure

// Namespaces
using namespace std;
namespace po = boost::program_options;

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    cout << "Press ENTER to continue...";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

/*!
************************************************
* Argument parser.
************************************************
*/
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")

        ("config,c",
         po::value<string>()->default_value("default"),
         "set configuration file path (default files defined for each test) "
         "...")

            ("verbose,v", po::value<string>()->implicit_value("0"), "verbosity level");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        cout << desc << "\n";
        return 0;
    }

    // Power supply object option
    if(vm.count("object")) { cout << "Object to initialize set to " << vm["object"].as<string>() << endl; }

    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    std::cout << "Initializing ..." << std::endl;

    std::string docPath = v_map["config"].as<string>();

    std::cout << "docPath" << docPath << std::endl;
    //    int         baudRate = fConfiguration.attribute("BaudRate").as_int();
    //    std::cout << "baudRate" << baudRate << std::endl;

    pugi::xml_document docSettings;

    DeviceHandler theHandler;
    theHandler.readSettings(docPath, docSettings);

    //    pugi::xml_parse_result result = docSettings.load_file(docPath.c_str());
    // docSettings.load_file(docPath.c_str());

    pugi::xml_node fDocumentRoot = docSettings.child("Devices");
    pugi::xml_node options       = fDocumentRoot.child("options");

    float       deltaV     = options.attribute("DeltaV").as_float();
    float       Vmax       = options.attribute("Vmax").as_float();
    std::string filename   = options.attribute("filename").value();
    float       deltaT     = options.attribute("DeltaT").as_float();
    float       compliance = options.attribute("Compliance").as_float();
    //    float Vrange=options.attribute("VRange").as_float();
    std::string pol_string   = options.attribute("Polarity").value();
    std::string panel_string = options.attribute("panel").value();
    int         polarity;
    float       dummy = 0;
    int         present_pol;
    float       voltage, current;
    float       Vrange;

    std::cout << "Polarity = " << pol_string << std::endl;
    std::cout << "Compliance = " << compliance << std::endl;
    //    std::cout << "VRange = " << Vrange << std::endl;
    std::cout << "DeltaT = " << deltaT << std::endl;
    std::cout << "DeltaV = " << deltaV << std::endl;
    std::cout << "Vmax = " << Vmax << std::endl;
    std::cout << "panel = " << panel_string << std::endl;
    std::cout << "filename = " << filename << std::endl;

    try
    {
        theHandler.getPowerSupply("MyKeithley");
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
    }

    if(pol_string.compare("positive") == 0)
        polarity = 1; // polarita' positiva
    else if(pol_string.compare("negative") == 0)
        polarity = -1; // polarita' negativa
    else
    {
        std::stringstream error;
        error << "polarity " << pol_string
              << " is not valid, "
                 "please check the xml configuration file.";
        throw std::runtime_error(error.str());
    }

    if(Vmax > 1000)
    {
        std::stringstream error;
        error << "Vmax " << Vmax
              << " is not valid, "
                 "please check the xml configuration file.";
        throw std::runtime_error(error.str());
    }

    FILE* f_data = fopen(filename.c_str(), "w+");
    if(f_data == 0)
    {
        char  buffer[256];
        char* errorMsg = strerror_r(errno, buffer, 256);
        printf("Error while opening %s: %s\n", filename.c_str(), errorMsg);
        return -1;
    }

#ifdef PLOT
    int n_points;
    n_points    = Vmax / deltaV;
    float* x    = new float[n_points];
    float* y    = new float[n_points];
    FILE*  pipe = popen("gnuplot", "w");
#endif

    // configure source voltage
    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("command :source:function volt", dummy);

    // configure sense current
    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("command :sense:function \"curr\"", dummy);

    // check if output is on
    if(theHandler.getPowerSupply("MyKeithley")->getChannel("master")->isOn())
    {
        voltage = theHandler.getPowerSupply("MyKeithley")->getChannel("master")->getOutputVoltage();

        if(voltage != 0)
        {
            std::cout << "Warning Voltage is on, ramping down" << std::endl;
            if(voltage < 0)
                present_pol = -1;
            else
                present_pol = 1;

            deltaV = deltaV * present_pol;
            while(voltage * present_pol > 0)
            {
                theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setVoltage(voltage);
                usleep(deltaT * 3e5);
                voltage -= deltaV;
            }
            theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setVoltage(0);
            deltaV = deltaV * present_pol;
        }
        theHandler.getPowerSupply("MyKeithley")->getChannel("master")->turnOff();
    }

    // set active panel
    if(panel_string.compare("front") == 0)
        theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("rear", false);
    else if(panel_string.compare("rear") == 0)
        theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("rear", true);
    else
    {
        std::stringstream error;
        error << "polarity " << pol_string
              << " is not valid, "
                 "please check the xml configuration file.";
        throw std::runtime_error(error.str());
    }

    // set compliance
    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setCurrentCompliance(compliance);

    // set range at maximum possible value

    if(Vmax <= 0.2)
        Vrange = 0.2;
    else if(Vmax <= 20)
        Vrange = 20;
    else if(Vmax <= 200)
        Vrange = 200;
    else
        Vrange = 1000;

    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("Vsrc_range", Vrange);

    // configure voltage source range
    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("Vsrc_range", Vrange);

    // configure current range auto
    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("autorange", false);

    deltaV = deltaV * polarity;
    Vmax   = Vmax * polarity;

    std::cout << "Starting measurement" << std::endl;

#ifdef PLOT
    char line_gnu[50];
    fprintf(pipe, "set xrange [0:%f]\n", Vmax * 1.1);
    fprintf(pipe, "unset key\n");
    //	fprintf(pipe,"set autoscale x\n");
    fprintf(pipe, "set autoscale y\n");
    fprintf(pipe, "set offset graph 0.10, 0.10\n");

    sprintf(line_gnu, "set format y \"%%.1e\"\n");
    fprintf(pipe, "%s", line_gnu);
    fprintf(pipe, "set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2 pointtype 7 pointsize 1.5\n");

    fprintf(pipe, "set xlabel \"Voltage [V]\"\n");
    fprintf(pipe, "show xlabel\n");
    fprintf(pipe, "set ylabel \"Current [A]\"\n");
    fprintf(pipe, "show ylabel\n");
#endif

    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->turnOn();
    voltage = theHandler.getPowerSupply("MyKeithley")->getChannel("master")->getOutputVoltage();

    for(int i = 0; i < Vmax / deltaV; i++)
    {
        if(deltaV * (i + 1) * polarity > Vrange) { std::cout << "Error, Voltage out of range" << std::endl; }

        theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setVoltage(deltaV * (i + 1));

        usleep(deltaT * 1e6);

        current = theHandler.getPowerSupply("MyKeithley")->getChannel("master")->getCurrent();
        voltage = theHandler.getPowerSupply("MyKeithley")->getChannel("master")->getOutputVoltage();

        fprintf(f_data, "%e %e\n", voltage, current);
#ifdef PLOT
        x[i] = deltaV * (i + 1);
        y[i] = current;

        fprintf(pipe, "plot '-' with linespoints linestyle 1\n");
        //		fprintf(pipe, "%f %f\n", x[i], y[i]);

        for(int ii = 0; ii <= i; ii++) { fprintf(pipe, "%f %e\n", x[ii], y[ii]); }

        fprintf(pipe, "e\n"); // finally, e
        fflush(pipe);         // flush the pipe to update the plot
#endif
    }

    std::cout << "Measurement done, press enter to continue" << std::endl;

    getchar();

    std::cout << "Ramping down" << std::endl;

    while(voltage * polarity > 0)
    {
        theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setVoltage(voltage);
        voltage -= deltaV;
        usleep(deltaT * 3e5);
    }

    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setVoltage(0);
    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->turnOff();

    theHandler.getPowerSupply("MyKeithley")->getChannel("master")->setParameter("command :system:local", dummy);

#ifdef PLOT
#ifdef SAVE

    fprintf(pipe, "set term 'pngcairo'\n");
    fprintf(pipe, "set output '%s.png'\n", filename.c_str());
    fprintf(pipe, "replot\n");
    fprintf(pipe, "unset output\n");
#endif

    fclose(pipe);
#endif

    fclose(f_data);

    return 0;
}
