#!/bin/sh
export PATH=$PATH:$PWD/bin
alias formatAll="find ./ -iname '*.h' -o -iname '*.cc' | xargs clang-format -i"
